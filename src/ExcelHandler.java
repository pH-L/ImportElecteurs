import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.format.*;
import org.apache.poi.ss.usermodel.*;
//import org.apache.poi.xssf.usermodel.*;

public class ExcelHandler {

	private DataFormatter formatter;
	
	// Tableau des headers : Clé -> n°colonne 
	private String headerNames[][] = {
			{
				"Matricule",
				"Nom",
				"Prénom",
				"Date de naissance",
				"Droits de vote"
			},
			{
				"id",
				"lastname",
				"firstname",
				"birthdate",
				"ddv"
			}
	}; 
	
	private String[] result;
	private String filelocation;
	private FileInputStream inputstream;
	private Workbook workbook;
	private Map<String, int[]> headerPos;
	
    public ExcelHandler(String filelocation) {
		this.filelocation = filelocation;
		this.formatter = new DataFormatter();
	}

	public String[] performExcel() throws IOException, InvalidFormatException
    {
		this.result = new String[10];
		this.result[0] = "ok";
		this.headerPos = new HashMap<String, int[]>();
		
    	this.openFile();
    	// Ouvre le workbook
    	this.workbook = WorkbookFactory.create(this.inputstream);
    	
    	// Première feuille
    	Sheet sheet = this.workbook.getSheetAt(0);
    	// Association des erreurs à la ligne
    	Map<Integer, ArrayList<ArrayList<String>>> report = new HashMap<>();
    	
    	// Parcours les lignes
    	int firstRowNumber = sheet.getFirstRowNum();
    	int lastRowNumber = sheet.getLastRowNum();
    	System.out.println("First row: " + firstRowNumber + ", Last row: " + lastRowNumber);
    	for(int rowNum = firstRowNumber; rowNum <= lastRowNumber; rowNum++)
    	{
    		// Rapport des erreurs pour la ligne
        	ArrayList<String> errors = new ArrayList<>();
        	ArrayList<String> warning = new ArrayList<>();
        	ArrayList<ArrayList<String>> rowReport = new ArrayList<ArrayList<String>>(2);
        	rowReport.add(0, errors);
        	rowReport.add(1, warning);
        	
    		Row row = sheet.getRow(rowNum);

    		// Première ligne : header. Prend la première ligne avec du contenu
    		if(rowNum == firstRowNumber)
    		{
    			checkHeaders(row);
            	if(this.result[0].equals("nok")) {
                	return this.result;
                }
            	displayHeaderPos();
    		}
    		else
    		{
    			rowReport = checkRowContent(row, rowReport);
    			if(!rowReport.get(0).isEmpty() || !rowReport.get(1).isEmpty()) {
    				report.put(rowNum, rowReport);
    			}
    			displayRowReport(rowNum, rowReport);
    		}
    		
    		
    	}
    	
    	// Ajout des erreurs dans le fichier
    	if(!report.isEmpty()) {
    		// Création des colonnes
        	insertColumnsBefore(0, 0, 2);
        	Row header = this.workbook.getSheetAt(0).getRow(0);
        	header.getCell(0).setCellValue("Statut");
        	header.getCell(1).setCellValue("Anomalie(s)");
        	
        	// Ajout des erreurs
        	for (Map.Entry<Integer, ArrayList<ArrayList<String>>> rowReport : report.entrySet()) {
        		insertRowErrors(0, rowReport.getKey(), rowReport.getValue());
        	}
    	}
        
    	close();
        return this.result;
    }
        
    public void openFile() throws IOException
    {
    	this.inputstream = new FileInputStream(new File(this.filelocation));
    }
    
    
    // **********************************************
    // CONTROL FUNCTIONS
    // **********************************************
        
    /**
     * - Vérifie les headers
     * - Enregistre leur position
     * @param row Ligne header
     */
    public void checkHeaders(Row headerRow)
    {
    	// Ligne vide ?
        if(headerRow == null) {
        	this.result[0] = "nok";
        	this.result[1] = "Empty header row";
        	return;
        }
        
        // En partant des colonnes attendues, cherche la valeur dans les cellules de la première ligne
        // - L'itérateur ignore les cellules vides, pas genant dans ce cas.
        for(int j=0; j < this.headerNames[0].length; j++) {
        	
        	boolean found = false;
        	
            for(Cell cell : headerRow) {
                	String headerName = cell.getStringCellValue();
                	
                	if(headerName.equals(this.headerNames[0][j])) {
                		int[] posArray = new int[2];
                		posArray[0] = cell.getColumnIndex();
                		posArray[1] = j;
                    	this.headerPos.put(this.headerNames[1][j], posArray);
                		found = true;
                    }                
        	}
        	
        	// Pas trouvé...
        	if(!found) {
        		this.result[0] = "nok";
        		this.result[1] = "Missing header";
            	if(result[2] == null) {
            		this.result[2] = this.headerNames[0][j];
            	} else {
            		this.result[2] = result[2].concat(" " + this.headerNames[0][j]);
            	}
            	return;
        	}
        }
    }
    
    /**
     * Check le contenu de la ligne
     * @param row ligne
     */
    public ArrayList<ArrayList<String>> checkRowContent(Row row, ArrayList<ArrayList<String>> report)
    {
    	// Parcours les colonnes attendues
    	for (Map.Entry<String, int[]> col : this.headerPos.entrySet())
    	{
    		String key = col.getKey();
    		int cellNum = col.getValue()[0];
    		int nameNum = col.getValue()[1];
    		Cell cell = row.getCell(cellNum, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);

    		// Améliorer le switch avec pourquoi pas un appel dynamique des fonctions basé sur les id
    		switch (key)
    		{
    			case "id":
    		        report = checkIdCell(cell, nameNum, report);		    
    				break;        
    			case "lastname":
    				report = checkLastNameCell(cell, nameNum, report);
    				break;
    			case "firstname":
    				report = checkFirstNameCell(cell, nameNum, report);
    				break;
    			case "birthdate":
    				report = checkPhoneCell(cell, nameNum, report);
    				break;
    			case "ddv":
    				System.out.println("-> " + key + " : pas de fonction");
    				break;
    			default:
    				throw new RuntimeException("Pas de fonction de controle pour le champ : " + key);
    		}	
    	}
    	
    	
    	// Retour
    	return report;
    }

    public ArrayList<ArrayList<String>> checkIdCell(Cell cell, int nameNum, ArrayList<ArrayList<String>> report)
    {
    	if(cell == null) {
    		report.get(0).add("Le champ " + this.headerNames[0][nameNum] + " est vide");
    	}

    	return report;
    }
    
    public ArrayList<ArrayList<String>> checkLastNameCell(Cell cell, int nameNum, ArrayList<ArrayList<String>> report)
    {
    	if(cell == null) {
    		report.get(0).add("Le champ " + this.headerNames[0][nameNum] + " est vide");
    	} else {
    		String data = this.formatter.formatCellValue(cell);
    		if(data.length() < 2) {
    			report.get(1).add("Le champ " + this.headerNames[0][nameNum] + " semble court");
    		}
    	}
    	
    	return report;
    }
    
    public ArrayList<ArrayList<String>> checkFirstNameCell(Cell cell, int nameNum, ArrayList<ArrayList<String>> report)
    {
    	if(cell == null || (cell != null && cell.getStringCellValue().isEmpty())) {
    		report.get(0).add("Le champ " + this.headerNames[0][nameNum] + " est vide");
    	} else {
    		String data = this.formatter.formatCellValue(cell);
    		if(data.length() < 2) {
    			report.get(1).add("Le champ " + this.headerNames[0][nameNum] + " semble court");
    		}
    	}
    	
    	return report;
    }
    
    public ArrayList<ArrayList<String>> checkPhoneCell(Cell cell, int nameNum, ArrayList<ArrayList<String>> report)
    {
    	if(cell == null) {
    		report.get(0).add("Le champ " + this.headerNames[0][nameNum] + " est vide");
    	} else {
    		String stringFormat = cell.getCellStyle().getDataFormatString();
    		CellNumberFormatter fmt = new CellNumberFormatter(stringFormat);
    		String phoneNumber = fmt.format(cell.getNumericCellValue());
        	
        	report.get(0).add("Le champ " + this.headerNames[0][nameNum] + " est :<" + phoneNumber + ">");
    	}
    	
    	
    	return report;
    }

    
    // **********************************************
    // DISPLAYS
    // **********************************************
    
    public void displayHeaderPos()
    {
    	System.out.println("----");
    	for (Map.Entry<String, int[]> col : this.headerPos.entrySet()) {
    	    System.out.println("Key = " + col.getKey() + ", Column n°" + col.getValue()[0] + ", Name n°" + col.getValue()[1]);
    	}
    	System.out.println("----");
    }
    
    public void displayRowReport(int rowNum, ArrayList<ArrayList<String>> report)
    {
    	System.out.println("**** ROW " +rowNum+ " REPORT ****");
    	System.out.println("--- Errors");
    	for(int i=0; i < report.get(0).size(); i++) {
    		System.out.println("- " + report.get(0).get(i));
    	}
    	System.out.println("--- Warning");
    	for(int i=0; i < report.get(1).size(); i++) {
    		System.out.println("- " + report.get(1).get(i));
    	}
    	System.out.println("***********************************");
    }
    
    // **********************************************
    // UTILITIES
    // **********************************************
    
    public int getNumberOfRows(int sheetIndex) {
		assert workbook != null;

		int sheetNumber = workbook.getNumberOfSheets();

		System.out.println("Found " + sheetNumber + " sheets.");

		if (sheetIndex >= sheetNumber) {
			throw new RuntimeException("Sheet index " + sheetIndex
					+ " invalid, we have " + sheetNumber + " sheets");
		}

		Sheet sheet = workbook.getSheetAt(sheetIndex);

		int rowNum = sheet.getLastRowNum() + 1;

		System.out.println("Found " + rowNum + " rows.");

		return rowNum;
	}
    
    public int getNumberOfColumns(int sheetIndex) {
		assert workbook != null;

		Sheet sheet = workbook.getSheetAt(sheetIndex);

		// get header row
		Row headerRow = sheet.getRow(0);
		int nrCol = headerRow.getLastCellNum();

		System.out.println("Found " + nrCol + " columns.");
		return nrCol;

	}
    
    /**
     * ATTENTION ne prend pas en charge les cellules avec des formules
     * @param sheetIndex
     * @param columnIndex
     * @param columnsToAdd
     */
    public void insertColumnsBefore(int sheetIndex, int columnIndex, int columnsToAdd)
    {
    	CreationHelper createHelper = this.workbook.getCreationHelper();
    	
    	Sheet sheet = this.workbook.getSheetAt(sheetIndex);
    	int nbRows = getNumberOfRows(sheetIndex);
    	int nbCols = getNumberOfColumns(sheetIndex); // Index of the last cell +1
    	System.out.println("Inserting " + columnsToAdd + " new column(s) at " + columnIndex);
    	
    	// Parcours les lignes
    	for(int r = 0; r < nbRows; r++) {
    		Row row = sheet.getRow(r);
    		
    		if (row == null) {
    			continue;
    		}
    		
    		// Décale vers la droite chaque cellule d'autant que nécessaire
    		
    		for(int c = nbCols+(columnsToAdd-1); c > columnIndex; c--) {
    			Cell rightCell = row.getCell(c);
    			
				if (rightCell != null) {
					row.removeCell(rightCell);
				}

				if((c - columnsToAdd) >= 0) {
					Cell leftCell = row.getCell(c - columnsToAdd);
					if (leftCell != null) {
						Cell newCell = row.createCell(c, leftCell.getCellTypeEnum());
						cloneCell(newCell, leftCell);
						
						/*
						if (newCell.getCellTypeEnum() == CellType.FORMULA) {
							
							newCell.setCellFormula(ExcelHelper.updateFormula(newCell.getCellFormula(), columnIndex));
							evaluator.notifySetFormula(newCell);
							CellValue cellValue = evaluator.evaluate(newCell);
							evaluator.evaluateFormulaCell(newCell);
							System.out.println(cellValue);
						}
						*/
					}
				}
    		}
    		
    		//Supprime les colonnes libérées et créé les nouvelles
			CellType blankCellType = CellType.BLANK;
			
			for(int i=columnIndex; i < (columnIndex+columnsToAdd) ; i++) {
				Cell currentEmptyCell = row.getCell(i);
				if(currentEmptyCell != null) {
					row.removeCell(currentEmptyCell);
				}
				row.createCell(i, blankCellType);
			}
    	}
    }
    
    /**
     * Insert les erreurs à la ligne souhaitée. Le fichier doit etre pret avec les colonnes nécessaires.
     * @param indexSheet
     * @param rowReport
     */
    private void insertRowErrors(int sheetIndex, int rowIndex, ArrayList<ArrayList<String>> rowReport)
    {
    	Sheet sheet = this.workbook.getSheetAt(sheetIndex);
    	Row row = sheet.getRow(rowIndex);
    	
    	Cell statusCell = row.getCell(0);
    	Cell infoCell = row.getCell(1);
    	
    	// Error style
    	CellStyle errorStyle = this.workbook.createCellStyle();;
    	errorStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
    	errorStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
    	// Warning style
    	CellStyle warningStyle = this.workbook.createCellStyle();
    	warningStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
    	warningStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
    	
    	// Détermine le statut
    	if(!rowReport.get(0).isEmpty()) {
    		statusCell.setCellValue("Bloquante");
    		statusCell.setCellStyle(errorStyle);
    	} else {
    		if(!rowReport.get(1).isEmpty()) {
    			statusCell.setCellValue("Warning");
    			statusCell.setCellStyle(warningStyle);
    		}
    	}
    	
    	// Ecrit les anomalies
    	String text = new String();
    	// Erreur
    	int type = 0;
    	int cpt = 0; // Compte le nombre de lignes qu'il faudra affiche dans la ligne
    	
    	for(int i=0; i < rowReport.get(type).size(); i++) {
			text += rowReport.get(type).get(i) +"\n";
			cpt++;
		}
    	// Warning
    	type = 1;
    	for(int i=0; i < rowReport.get(type).size(); i++) {
			text += rowReport.get(type).get(i) + "\n";
			cpt++;
		}
    	
    	CellStyle infoStyle = this.workbook.createCellStyle();
    	infoStyle.setWrapText(true);
    	infoStyle.setVerticalAlignment(VerticalAlignment.TOP);
    	infoCell.setCellStyle(infoStyle);
    	infoCell.setCellValue(text);
    	row.setHeightInPoints((cpt*sheet.getDefaultRowHeightInPoints())); // Hauteur de ligne optimale
    	sheet.autoSizeColumn(0); // Largeur de colonne optimale
    	sheet.autoSizeColumn(1); // Largeur de colonne optimale
    }
    
    /*
	 * Takes an existing Cell and merges all the styles and forumla into the new
	 * one
	 */
	private static void cloneCell(Cell cNew, Cell cOld) {
		cNew.setCellComment(cOld.getCellComment());
		cNew.setCellStyle(cOld.getCellStyle());

		switch (cOld.getCellTypeEnum())
		{
			case BOOLEAN: {
				cNew.setCellValue(cOld.getBooleanCellValue());
				break;
			}
			case NUMERIC: {
				cNew.setCellValue(cOld.getNumericCellValue());
				break;
			}
			case STRING: {
				cNew.setCellValue(cOld.getStringCellValue());
				break;
			}
			case ERROR: {
				cNew.setCellValue(cOld.getErrorCellValue());
				break;
			}
			case FORMULA: {
				cNew.setCellFormula(cOld.getCellFormula());
				break;
			}
		}
	}

    public void close() throws IOException {

		this.inputstream.close();
        FileOutputStream outputStream = new FileOutputStream(this.filelocation);
        this.workbook.write(outputStream);
        this.workbook.close();
        outputStream.close();

	}
}
