<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Import</title>
    </head>
<body>
	<h1>Import</h1>
	<p><strong>${file}</strong></p>
	<p>
		<ul>
			<c:forEach items="${result}" var="resultline">
				<li>
				<c:out value="${resultline}"/>
				</li>
			</c:forEach>
		</ul>
	</p>
	
	<form action="/VotersImport/index" method="post" enctype="multipart/form-data">
        <fieldset>
            <legend>Import</legend>

			<label for="file">Fichier XLSX</label>
            <input type="file" id="file" name="file" />
            <br/>
            
            <input type="submit" value="Envoyer" />
            <br />                
        </fieldset>
    </form>
</body>
</html>